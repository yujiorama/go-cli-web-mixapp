package main

import (
	"bitbucket.org/yujiorama/go-cli-web-mixapp/cmd/cli"
	"bitbucket.org/yujiorama/go-cli-web-mixapp/cmd/web"
	"flag"
	"log"
)

func main() {

	flag.Parse()

	if web.CanDo() {
		if err := web.Do(); err != nil {
			log.Fatalf("web.Do error: %v\n", err)
		}
		return
	}

	if err := cli.Do(); err != nil {
		log.Fatalf("cli.Do error: %v\n", err)
	}
}
