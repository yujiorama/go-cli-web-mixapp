package web

import (
	"flag"
	"fmt"
	"net/http"
)

var (
	serverEnable bool
)

func init() {
	flag.BoolVar(&serverEnable, "server", false, "server mode")
}

func CanDo() bool {

	return serverEnable
}

func Do() error {

	mux := http.NewServeMux()
	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		_, _ = w.Write([]byte("hello"))
	})

	server := &http.Server{
		Addr:    "0.0.0.0:8080",
		Handler: mux,
	}

	if err := server.ListenAndServe(); err != http.ErrServerClosed {
		return fmt.Errorf("HTTP server ListenAndServe: %v", err)
	}

	return nil
}
